﻿using UnityEngine;
using System.Collections.Generic;

public class GameSource : ScriptableObjectSingleton<GameSource>
{
    public List<GameObject> Prefabs = new List<GameObject>();
    public List<Enemy> EnemyPrefabs = new List<Enemy>();

    /// <summary>
    /// если false то при разрыве дистанции с игроком, уже начатый удар НЕ нанесет урон
    /// если true то при разрыве дистанции с игроком, уже начатый удар нанесет урон
    /// </summary>
    public bool AnywayHit = true;
}

