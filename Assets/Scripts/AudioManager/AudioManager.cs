﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class AudioManager : MonoBehaviourSingleton<AudioManager>
{
    public List<AudioPool> Pulls = new List<AudioPool>();

    [SerializeField]
    private List<AudioGroupVolumePair> AudioGroupVolumePairs = new List<AudioGroupVolumePair>();

    public void PlaySound(string name)
    {
        AudioSourceKeeper audioSourceKeeper = getSound(name);

        if (audioSourceKeeper)
        {
            audioSourceKeeper.Play();
        }
    }

    public void PlaySound(string name, Vector3 position)
    {
        transform.position = position;

        PlaySound(name);
    }

    public AudioSourceKeeper GetSound(string name)
    {
        AudioSourceKeeper audioSourceKeeper = getSound(name);

        if (audioSourceKeeper) audioSourceKeeper.IsBlocked = true;
       
        return audioSourceKeeper;
    }

    /// <summary>
    /// AudioGroup and volume 0.0 to 1.0
    /// </summary>
    /// <param name="audioGroup"></param>
    /// <param name="volume">0.0 to 1.0</param>
    public void SetVolume(AudioGroup audioGroup, float volume)
    {
        volume = volume.Normalized();
        getRegisteredAudioGroupVolumePair(audioGroup, volume);

        foreach (AudioPool item in Pulls)
        {
            foreach (AudioSourceKeeper audioSourceKeeper in item)
            {
                if (audioSourceKeeper.Group == audioGroup)
                {
                    audioSourceKeeper.Volume = volume;
                }
            }
        }
    }

    protected void addToPulls(AudioSourceKeeper audioSourceKeeper)
    {
        foreach (AudioPool item in Pulls)
        {
            if (item.AudioPoolType == audioSourceKeeper.AudioPoolType)
            {
                item.Add(audioSourceKeeper);
                return;//выходим после добавления в пул
            }
        }

        AudioPool audioPool = new AudioPool(audioSourceKeeper.AudioPoolType);
        Pulls.Add(audioPool);

        audioPool.Add(audioSourceKeeper);
    }

    protected AudioSourceKeeper getSound(string name)
    {
        AudioSourceKeeper audioSourceKeeper = null;
        foreach (AudioPool item in Pulls)
        {
            if (item == null) continue;
            audioSourceKeeper = item.Find(x => x && x.Name == name && !x.IsPlaying && !x.IsBlocked);
            if (audioSourceKeeper) break;
        }

        if (!audioSourceKeeper)
        {
            AudioSourceKeeper prefab = AudioLibrary.Instance.AudioSourceKeepers.Find(item => item.Name == name);
            if (prefab)
            {
                audioSourceKeeper = Instantiate(prefab);
                audioSourceKeeper.transform.SetParent(transform);

                audioSourceKeeper.Volume = getRegisteredAudioGroupVolumePair(audioSourceKeeper.Group).Volume;

                addToPulls(audioSourceKeeper);
            }
        }
        return audioSourceKeeper;
    }

    /// <summary>
    /// если AudioGroup не найдена создается новая с указанной громкостью
    /// </summary>
    /// <param name="audioGroup"></param>
    /// <param name="volume">0.0 to 1.0</param>
    /// <returns></returns>
    protected AudioGroupVolumePair getRegisteredAudioGroupVolumePair(AudioGroup audioGroup, float volume = 1)
    {
        AudioGroupVolumePair audioGroupVolumePair = AudioGroupVolumePairs.Find(item => item.AudioGroup == audioGroup);
        if (audioGroupVolumePair == null)
        {
            audioGroupVolumePair = new AudioGroupVolumePair(audioGroup, volume);
            AudioGroupVolumePairs.Add(audioGroupVolumePair);
        }
        return audioGroupVolumePair;
    }

    protected override void Awake()
    {
        base.Awake();

        //DontDestroyOnLoad(gameObject);
    }

    [Serializable]
    public class AudioPool : IEnumerable
    {
        public AudioPool(AudioPoolType audioPoolType)
        {
            AudioPoolType = audioPoolType;
            MaxSize = AudioLibrary.Instance.GetPullMaxSize(audioPoolType);
        }

        public AudioPoolType AudioPoolType = AudioPoolType.Effects;

        public int MaxSize = 100;

        protected List<AudioSourceKeeper> pool = new List<AudioSourceKeeper>();

        public void Clear(bool onlyStopped = false)
        {
            if (onlyStopped)
            {
                pool.RemoveAll(item => !item.IsPlaying);
            }
            else
            {
                pool.Clear();
            }
        }

        public AudioSourceKeeper Find(Predicate<AudioSourceKeeper> predicate)
        {
            return pool.Find(predicate);
        }

        public void Add(AudioSourceKeeper audioSourceKeeper)
        {
            if (pool.Count >= MaxSize)
            {
                AudioSourceKeeper toRemove = pool.Find(item => !item.IsPlaying);

                if (toRemove)
                {
                    pool.Remove(toRemove);
                }
                else if (pool.Count > 0)
                {
                    pool.Remove(pool[0]);
                }
            }

            pool.Add(audioSourceKeeper);
        }

        public void Remove(AudioSourceKeeper audioSourceKeeper)
        {
            pool.Remove(audioSourceKeeper);
        }

        public IEnumerator GetEnumerator()
        {
            return ((IEnumerable)pool).GetEnumerator();
        }
    }

    [Serializable]
    protected class AudioGroupVolumePair
    {
        public AudioGroupVolumePair(AudioGroup audioGroup, float volume)
        {
            AudioGroup = audioGroup;
            this.volume = volume.Normalized();
        }

        public AudioGroup AudioGroup;

        [SerializeField]
        private float volume = 1;
        public float Volume
        {
            get
            {
                return volume;
            }
            set
            {
                volume = value.Normalized();
            }
        }
    }
}

[Serializable]
public class AudioPoolTypeCountPair
{
    public AudioPoolType AudioPoolType;
    public int MaxSize;
}

