﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioLibrary : ScriptableObjectSingleton<AudioLibrary>
{
    public List<AudioSourceKeeper> AudioSourceKeepers = new List<AudioSourceKeeper>();

    public int DefaultPullMaxSize = 100;

    public List<AudioPoolTypeCountPair> AudioPoolTypeCountPairs = new List<AudioPoolTypeCountPair>();

    public int GetPullMaxSize(AudioPoolType audioPoolType)
    {
        AudioPoolTypeCountPair audioPoolTypeCountPair = AudioPoolTypeCountPairs.Find(item => item.AudioPoolType == audioPoolType);
        if (audioPoolTypeCountPair != null)
        {
            return audioPoolTypeCountPair.MaxSize;
        }
        return DefaultPullMaxSize;
    }
}
