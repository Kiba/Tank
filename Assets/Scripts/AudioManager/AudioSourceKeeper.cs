﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(AudioSource))]
public class AudioSourceKeeper : MonoBehaviour
{
    protected virtual void Awake()
    {
        if (!audioSource) audioSource = GetComponent<AudioSource>();

        baseVolume = audioSource.volume;
    } 

    public string Name;

    public AudioGroup Group = AudioGroup.Effects;

    public AudioPoolType AudioPoolType = AudioPoolType.Effects;

    [SerializeField]
    protected AudioSource audioSource;

    private float baseVolume = 0;

    private float volume;
    /// <summary>
    /// 0.0 to 1.0
    /// </summary>
    public float Volume
    {
        get
        {
            return volume;
        }
        set
        {
            volume = value.Normalized();

            audioSource.volume = baseVolume * volume;
        }
    }

    public float Pitch
    {
        get
        {
            return audioSource.pitch;
        }
        set
        {
            audioSource.pitch = value;
        }
    }

    private bool isBlocked;
    public bool IsBlocked
    {
        get
        {
            return isBlocked;
        }
        set
        {
            isBlocked = value;
        }
    }

    public bool IsPlaying
    {
        get
        {
            return audioSource.isPlaying;
        }
    }

    internal void Play()
    {
        audioSource.Play();
    }

}

public enum AudioGroup { Effects, Music }
public enum AudioPoolType { Effects, Menu, Music }

