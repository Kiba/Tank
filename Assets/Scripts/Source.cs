﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Source
{ 
    public static IEnumerator DeferredAction(float delay, Action action)
    {
        yield return new WaitForSeconds(delay);

        action();
    }
}

public static class Extensions
{
    public static T GetRandomItem<T>(this List<T> list)
    {
        if (list.Count == 0)
        {
            return default(T);
        }
        return list[UnityEngine.Random.Range(0, list.Count)];
    }

    public static bool AddOnce<T>(this List<T> list,T item)
    {
        if (!list.Contains(item))
        {
            list.Add(item);
            return true;
        }
        return false;
    }

    public static T GetRandomItem<T>(this List<T> list, Predicate<T> match)
    {
        return list.FindAll(match).GetRandomItem();
    }

    public static T GetAnother<T>(this List<T> list, T item)
    {
        return list.Find(x => x != null && !x.Equals(item));
    }

    public static T GetLast<T>(this List<T> list)
    {
        return list[list.Count - 1];
    }

    public static void RemoveNulls<T>(this List<T> list)
    {
        list.RemoveAll(item => item == null);
    }

    public static Color GetRandomColor()
    {
        return new Color(UnityEngine.Random.Range(0, 1f), UnityEngine.Random.Range(0, 1f), UnityEngine.Random.Range(0, 1f));
    }

    public static bool GetRandomBool()
    {
        return UnityEngine.Random.Range(0, 2) == 1 ? true : false;
    }

    /// <summary>
    /// return value not less 0 and not more than 1
    /// </summary>
    /// <returns>0.0 to 1.0</returns>
    public static float Normalized(this float value)
    {
        if (value > 1) value = 1;
        else if (value < 0) value = 0;

        return value;
    }

    public static Vector3 MoveTowards(this Vector3 current, Vector3 target, float maxDistanceDelta)
    {
        return Vector3.MoveTowards(current, target, maxDistanceDelta);
    }
}

public abstract class MonoBehaviourSingleton<T> : MonoBehaviour where T : MonoBehaviourSingleton<T>
{
    private static T instance;
    public static T Instance
    {
        get
        {
            if (!instance)
            {
                instance = FindObjectOfType<T>();
                if (!instance)
                {
                    GameObject gameObject = new GameObject(typeof(T).ToString());
                    instance = gameObject.AddComponent<T>();
                }
            }
            return instance;
        }
        protected set
        {
            if (!instance)
            {
                instance = value;
            }
            else if (value && instance != value)
            {
                Destroy(value);
            }
        }
    }

    /// <summary>
    /// обязательно вызвать базовай метод
    /// </summary>
    protected virtual void Awake()
    {
        Instance = this as T;
    }
}

public abstract class ScriptableObjectSingleton<T> : ScriptableObject where T : ScriptableObjectSingleton<T>
{
    private static T instance;
    public static T Instance
    {
        get
        {
            if (!instance)
            {
                string name = typeof(T).ToString();

                instance = Resources.Load<T>(name);

                if (!instance)
                {
                    instance = CreateAsset(name);
                }
            }

            return instance;
        }
    }

    public static T CreateAsset(string name)
    {
        T scriptableObject = CreateInstance<T>();
#if UNITY_EDITOR
        string filePath = "Assets/Resources/";

        if (!Directory.Exists(filePath)) Directory.CreateDirectory(filePath);

        UnityEditor.AssetDatabase.CreateAsset(scriptableObject, filePath + name + ".asset");
        UnityEditor.AssetDatabase.SaveAssets();
#endif
        return scriptableObject;
    }
}


[Serializable]
public class IntValueKeeper
{
    public IntValueKeeper()
    {

    }

    /// <summary>
    /// min == 0
    /// </summary>
    /// <param name="maxAndNow"></param>
    public IntValueKeeper(int maxAndNow)
    {
        max = maxAndNow;
        now = maxAndNow;
    }

    /// <summary>
    /// min == 0
    /// </summary>
    /// <param name="max"></param>
    /// <param name="now"></param>
    public IntValueKeeper(int max, int now)
    {
        this.max = max;
        this.now = now;
    }

    public IntValueKeeper(int max, int now, int min) : this(max, now)
    {
        this.min = min;
    }

    public static implicit operator int(IntValueKeeper intValueKeeper)
    {
        return intValueKeeper.Now;
    }

    public static IntValueKeeper operator +(IntValueKeeper intValueKeeper, int x)
    {
        intValueKeeper.Now += x;
        return intValueKeeper;
    }

    public static IntValueKeeper operator -(IntValueKeeper intValueKeeper, int x)
    {
        intValueKeeper.Now -= x;
        return intValueKeeper;
    }

    [SerializeField]
    protected int max = 0;
    public int Max
    {
        get
        {
            return max;
        }
        set
        {
            max = value;
        }
    }

    [SerializeField]
    protected int now = 0;
    public int Now
    {
        get
        {
            return now;
        }
        set
        {
            if (value <= Min)
            {
                now = Min;
            }
            else
            {
                now = value;
            }
        }
    }

    [SerializeField]
    protected int min = 0;
    public int Min
    {
        get
        {
            return min;
        }
        set
        {
            min = value;
        }
    }
}

[Serializable]
public class FloatValueKeeper
{
    public FloatValueKeeper()
    {

    }

    /// <summary>
    /// min == 0
    /// </summary>
    /// <param name="maxAndNow"></param>
    public FloatValueKeeper(float maxAndNow)
    {
        max = maxAndNow;
        now = maxAndNow;
    }

    /// <summary>
    /// min == 0
    /// </summary>
    /// <param name="max"></param>
    /// <param name="now"></param>
    public FloatValueKeeper(float max, float now)
    {
        this.max = max;
        this.now = now;
    }

    public FloatValueKeeper(float max, float now, float min) : this(max, now)
    {
        this.min = min;
    }

    public static implicit operator float(FloatValueKeeper floatValueKeeper)
    {
        return floatValueKeeper.Now;
    }

    public static FloatValueKeeper operator +(FloatValueKeeper intValueKeeper, float x)
    {
        intValueKeeper.Now += x;
        return intValueKeeper;
    }

    public static FloatValueKeeper operator -(FloatValueKeeper intValueKeeper, float x)
    {
        intValueKeeper.Now -= x;
        return intValueKeeper;
    }

    public event EventHandler OnMinReach;
    public event EventHandler<FloatEventArg> OnChanged;

    private void riseOnMinReach()
    {
        if (OnMinReach != null) OnMinReach(this, new EventArgs());
    }

    private void riseOnChanged(float value)
    {
        if (OnChanged != null) OnChanged(this, new FloatEventArg(value));
    }

    [SerializeField]
    protected float max = 0;
    public float Max
    {
        get
        {
            return max;
        }
        set
        {
            max = value;
        }
    }

    [SerializeField]
    protected float now = 0;
    public float Now
    {
        get
        {
            return now;
        }
        set
        {
            float delta = value - now;

            if (value <= Min)
            {
                now = Min;
                riseOnChanged(delta);
                riseOnMinReach();
            }
            else
            {
                now = value;
                riseOnChanged(delta);
            }
        }
    }

    [SerializeField]
    protected float min = 0;
    public float Min
    {
        get
        {
            return min;
        }
        set
        {
            min = value;
        }
    }
}

public class FloatEventArg : EventArgs
{
    public float Value;

    public FloatEventArg()
    {

    }

    public FloatEventArg(float value)
    {
        Value = value;
    }
}

public class ColliderEventArgs : EventArgs
{
    public ColliderEventArgs()
    {

    }

    public ColliderEventArgs(Collider other)
    {
        OtherCollider = other;
    }

    public Collider OtherCollider;
}

[Serializable]
public struct StringIntPair
{
    public StringIntPair(string str, int value)
    {
        String = str;
        Int = value;
    }

    public string String;
    public int Int;
}

[Serializable]
public struct Point
{
    public int X;
    public int Y;

    public Point(int x, int y)
    {
        X = x;
        Y = y;
    }

    public static Point operator +(Point a, Point b)
    {
        return new Point(a.X + b.X, a.Y + b.Y);
    }

    public static Point operator -(Point a, Point b)
    {
        return new Point(a.X - b.X, a.Y - b.Y);
    }

    public static bool operator ==(Point a, Point b)
    {
        return a.X == b.X && a.Y == b.Y;     
    }

    public static bool operator !=(Point a, Point b)
    {
        return a.X != b.X || a.Y != b.Y;
    }

    public override bool Equals(object obj)
    {
        if (obj is Point)
        {
            return this == (Point)obj;
        }
        return false;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
}
