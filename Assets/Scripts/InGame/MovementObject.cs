﻿using UnityEngine;
using System;

public abstract class MovementObject : MonoBehaviour
{
    public event EventHandler OnDisintegrate;

    public bool CliarEvents = true;

    [SerializeField]
    private bool moveActive = true;
    public virtual bool MoveActive
    {
        get
        {
            return moveActive;
        }

        set
        {
            moveActive = value;
        }
    }

    public float MoveSpeed = 1;

    protected virtual void FixedUpdate()
    {
       if(MoveActive) move();
    }

    protected abstract void move();

    public virtual void Disintegrate()
    {
        riseOnDisintegrate();
        if (CliarEvents) OnDisintegrate = null;
    }

    protected virtual void riseOnDisintegrate()
    {
        if (OnDisintegrate != null) OnDisintegrate(this, new EventArgs());
    }
}