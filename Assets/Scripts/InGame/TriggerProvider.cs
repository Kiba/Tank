﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// обязательно повесить хоть какой-то коллайдер
/// </summary>
[RequireComponent(typeof(Collider))]
public class TriggerProvider : MonoBehaviour
{
    [SerializeField]
    private Collider thisCollider;
    public bool ColliderEnabled
    {
        get
        {
            //if (!thisCollider) thisCollider = GetComponent<Collider>();
            return thisCollider.enabled;
        }
        set
        {
            //if (!thisCollider) thisCollider = GetComponent<Collider>();
            thisCollider.enabled = value;
        }
    }

    [SerializeField]
    private bool cacheColision = false;
    public bool CacheColision
    {
        get
        {
            return cacheColision;
        }
        set
        {
            cacheColision = value;
            if (!value)
            {
                colliders.Clear();
            }
        }
    }

    public event EventHandler<ColliderEventArgs> TriggerEnter;
    public event EventHandler<ColliderEventArgs> TriggerStay;
    public event EventHandler<ColliderEventArgs> TriggerExit;

    private List<Collider> colliders = new List<Collider>();
    public List<Collider> Colisions
    {
        get
        {
            return new List<Collider>(colliders);
        }
    } 

    public List<Collider> GetAllByTag(string tag)
    {
        //colliders.RemoveNulls();
        return colliders.FindAll(item => item && item.tag == tag);
    }

    public List<Collider> GetAllInSphereCastByTag(string tag)
    {
        List<Collider> collidersInSphere = new List<Collider>();

        SphereCollider thisSphereCollider = GetComponent<SphereCollider>();

        if (thisSphereCollider)
        {
            collidersInSphere = new List<Collider>(Physics.OverlapSphere(transform.position, thisSphereCollider.radius));
        }

        return collidersInSphere.FindAll(item => item && item.tag == tag);
    }

    public void Awake()
    {
        if (!thisCollider) thisCollider = GetComponent<Collider>();
    }

    private void OnTriggerEnter(Collider other)
    {
        riseTriggerEnter(other);
        if (cacheColision) colliders.Add(other);
    }

    private void OnTriggerStay(Collider other)
    {
        riseTriggerStay(other);
    }

    private void OnTriggerExit(Collider other)
    {
        riseTriggerExit(other);
        if (cacheColision) colliders.Remove(other);
    }

    private void riseTriggerEnter(Collider other)
    {
        if (TriggerEnter != null) TriggerEnter(this, new ColliderEventArgs(other));
    }

    private void riseTriggerStay(Collider other)
    {
        if (TriggerStay != null) TriggerStay(this, new ColliderEventArgs(other));
    }

    private void riseTriggerExit(Collider other)
    {
        if (TriggerExit != null) TriggerExit(this, new ColliderEventArgs(other));
    }
}

