﻿using System.Collections;
using UnityEngine;

public class Enemy : Unit
{
    public string EnemyType = "";

    public float Damage;

    public float HitDelay;
    public float ReloadTime;

    public float DeathDelay;

    public Collider BodyCollider;
    public Animator ThisAnimator;
    public TriggerProvider Observer;
    public NavMeshAgent Agent;
    public Player Target;

    public override bool MoveActive
    {
        get
        {
            return base.MoveActive;
        }
        set
        {
            base.MoveActive = value;
            if (value)
            {
                Agent.Resume();
            }
            else
            {
                Agent.Stop();
            }

            ThisAnimator.SetBool("Move", value);
        }
    }

    protected bool inAttack = false;

    public void Awake()
    {
        Observer.TriggerEnter += Observer_TriggerEnter;
        Observer.TriggerExit += Observer_TriggerExit;

        Agent.speed = MoveSpeed;

        Target = GameConrtoller.Instance.CurrentPlayer;
    }

    public override void Disintegrate()
    {
        base.Disintegrate();

        StartCoroutine(delayedDeath());

        AudioManager.Instance.PlaySound("Death");
    }

    protected virtual void OnEnable()
    {
        MoveActive = true;
        BodyCollider.enabled = true;
        Observer.gameObject.SetActive(true);
        ThisAnimator.SetBool("Dead", false);
        HitPoints = MaxHitPoints;
    }

    protected override void move()
    {
        if (Target)
        {
            Agent.destination = Target.transform.position;
        }
    }

    private void Observer_TriggerEnter(object sender, ColliderEventArgs e)
    {
        if (e.OtherCollider.tag == "Player" && !inAttack)
        {
            StartCoroutine(attack());
        }
    }

    private void Observer_TriggerExit(object sender, ColliderEventArgs e)
    {
        if (e.OtherCollider.tag == "Player")
        {
            inAttack = false;
        }
    }

    private IEnumerator delayedDeath()
    {
        MoveActive = false;
        BodyCollider.enabled = false;
        Observer.gameObject.SetActive(false);

        if (DeathDelay > 0)
        {
            ThisAnimator.SetBool("Dead", true);
            yield return new WaitForSeconds(DeathDelay);
            ThisAnimator.SetBool("Dead", false);
        }

        Pull.Instance.AddToPull(this);
    }
    protected virtual IEnumerator attack()
    {
        MoveActive = false;
        inAttack = true;

        while (Target && inAttack)
        {
            ThisAnimator.SetBool("Attack", true);
            yield return new WaitForSeconds(HitDelay);
            ThisAnimator.SetBool("Attack", false);

            if (Target && (inAttack || GameSource.Instance.AnywayHit))
            {
                Target.SetDamage(Damage);
            }

            yield return new WaitForSeconds(ReloadTime - HitDelay);
        }
        inAttack = false;
        MoveActive = true;
    }
}

