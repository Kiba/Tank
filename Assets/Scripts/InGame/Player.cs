﻿using UnityEngine;

public class Player : Unit
{
    public Weapon ActiveWeapon;

    public float TurnSpeed = 2;

    public string EngineSoundName = "Engine";

    public Vector3 ZeroPoint = new Vector3();
    public float MoveRadius = 50;

    private int weaponIndex = 0;

    [SerializeField]
    private Transform weaponPoint = null;

    [SerializeField]
    private Weapon[] weapons = new Weapon[0];

    private AudioSourceKeeper engine;

    public void ChangeWeapon(int direction)
    {
        direction = direction >= 0 ? 1 : -1;

        weapons[weaponIndex].gameObject.SetActive(false);

        weaponIndex += direction;

        if (weaponIndex >= weapons.Length) weaponIndex = 0;
        else if (weaponIndex < 0) weaponIndex = weapons.Length - 1;

        weapons[weaponIndex].gameObject.SetActive(true);

        ActiveWeapon = weapons[weaponIndex];
    }

#if UNITY_EDITOR
    [ContextMenu("initialiseWeapons")]
    private void initialiseWeapons()
    {
        Weapon[] prefabs = weapons;
        weapons = new Weapon[prefabs.Length];

        for (int i = 0; i < prefabs.Length; i++)
        {
            Weapon instanse = Instantiate(prefabs[i], weaponPoint, false) as Weapon;
            instanse.enabled = false;

            weapons[i] = instanse;
        }

        weaponIndex = 0;
        weapons[0].enabled = true;
        ActiveWeapon = weapons[0];
    }
#endif
    private void setWeapon(Weapon weapon)
    {
        if (weapon == ActiveWeapon) return;

        foreach (Weapon item in weapons)
        {
            if (item) item.gameObject.SetActive(false);
        }
        weapon.gameObject.SetActive(true);

        ActiveWeapon = weapon;
    }

    protected virtual void Start()
    {
        engine = AudioManager.Instance.GetSound(EngineSoundName);
    }

    public override void Disintegrate()
    {
        base.Disintegrate();

        Destroy(gameObject);
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.X))
        {
            ActiveWeapon.Shoot();
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            ChangeWeapon(1);
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            ChangeWeapon(-1);
        }
    }

    protected override void move()
    {
        bool isMove = false;
        bool isTurn = false;
        bool moveBack = false;

        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            Vector3 targetPoint = transform.position + transform.forward * (Time.deltaTime * MoveSpeed);
            if (Vector3.Distance(ZeroPoint, targetPoint) < 50)
            {
                transform.position = targetPoint;
            }

            isMove = true;
        }

        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            Vector3 targetPoint = transform.position - transform.forward * (Time.deltaTime * MoveSpeed);
            if (Vector3.Distance(ZeroPoint, targetPoint) < 50)
            {
                transform.position = targetPoint;
            }

            isMove = moveBack = true;
        }

        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            float turnDirection = (Time.deltaTime * TurnSpeed);
            if (moveBack) turnDirection = -turnDirection;

            transform.eulerAngles += new Vector3(0, turnDirection, 0);

            isTurn = true;
        }

        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            float turnDirection = -(Time.deltaTime * TurnSpeed);
            if (moveBack) turnDirection = -turnDirection;

            transform.eulerAngles += new Vector3(0, turnDirection, 0);

            isTurn = true;
        }

        if (isMove || isTurn)
        {
            engine.Pitch = 2;
        }
        else
        {
            engine.Pitch = 1;
        }
    }
}

