﻿using UnityEngine;
using System.Collections;

public class CameraMover : MonoBehaviour
{
    private Transform playerTransform;

    private void Start()
    {
        playerTransform = GameConrtoller.Instance.CurrentPlayer.transform;
    }

    private void Update()
    {
        if (playerTransform)
        {
            transform.position = new Vector3(playerTransform.position.x, transform.position.y, playerTransform.position.z);
        }
    }
}