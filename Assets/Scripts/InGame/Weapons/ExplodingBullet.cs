﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ExplodingBullet : Bullet
{
    public float Radius;

    public float DeathDelay = 2;

    [SerializeField]
    new private ParticleSystem particleSystem = null;

    protected override void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy" || other.tag == "Obstacle")
        {
            particleSystem.Play();
            Collider[] colliders = Physics.OverlapSphere(transform.position, Radius);
            foreach (Collider item in colliders)
            {
                if (item.tag == "Enemy")
                {
                    Enemy enemy = item.GetComponent<Enemy>();
                    enemy.SetDamage(Damage);
                }
            }
            Disintegrate();
        }      
    }

    public override void Disintegrate()
    {
        if (lifeCoroutine != null)
        {
            StopCoroutine(lifeCoroutine);
        }

        MoveActive = false;

        StartCoroutine(Source.DeferredAction(DeathDelay, base.Disintegrate));
    }
}