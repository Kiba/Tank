﻿using UnityEngine;
using System.Collections;

public class GaussBullet : Bullet
{
    protected override void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            Enemy enemy = other.GetComponent<Enemy>();

            float forDisintegrate = enemy.HitPoints / enemy.Armor;
            if (Damage - forDisintegrate >= 0)
            {
                enemy.SetDamage(forDisintegrate);
            }
            else
            {
                enemy.SetDamage(Damage);
            }

            Damage -= forDisintegrate;

            if (Damage <= 0)
            {
                Disintegrate();
            }         
        }
        else if (other.tag == "Obstacle")
        {
            Disintegrate();
        }
    }
}
