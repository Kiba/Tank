﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class Weapon : MonoBehaviour
{
    public Bullet BulletPrefab;

    public float ReloadTime = 0.1f;

    public string ShotSound = "Shot00";

    [SerializeField]
    private Transform shotPoint = null;

    private List<Bullet> pull = new List<Bullet>();

    private bool baseReloaded = true;
    private bool reloaded
    {
        get
        {
            return baseReloaded;
        }
        set
        {
            baseReloaded = value;
            if (!value)
            {
                StartCoroutine(Source.DeferredAction(ReloadTime, delegate ()
                {
                    reloaded = true;
                }));
            }
        }
    }

    /// <summary>
    /// Shoot if reload
    /// </summary>
    public virtual void Shoot()
    {
        if (reloaded)
        {
            shoot();
            reloaded = false;
        }
    }

    private void shoot()
    {
        Bullet bullet;

        if (pull.Count > 0)
        {
            bullet = pull[0];
            pull.Remove(bullet);

            bullet.Shot(shotPoint.position, shotPoint.rotation);
        }
        else
        {
            bullet = Instantiate(BulletPrefab, shotPoint.position, shotPoint.rotation) as Bullet;
            bullet.OnDisintegrate += Bullet_OnDisintegrate;

            bullet.Shoot();

#if UNITY_EDITOR
            //так удобнее в редакторе, но в приложении будет лишней операцией
            bullet.transform.SetParent(GameConrtoller.Instance.transform);
#endif
        }
        AudioManager.Instance.PlaySound(ShotSound);
    }

    private void OnEnable()
    {
        reloaded = true;
    }

    private void Bullet_OnDisintegrate(object sender, EventArgs e)
    {
        Bullet bullet = sender as Bullet;

        pull.Add(bullet);
    }
}