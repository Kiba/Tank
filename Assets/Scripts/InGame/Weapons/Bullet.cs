﻿using UnityEngine;

public class Bullet : MovementObject
{
    public float Damage = 1;

    [SerializeField]
    private float lifeTime = 1;

    protected Coroutine lifeCoroutine;

    private Vector3 forward;

    public void OnEnable()
    {
        lifeCoroutine = StartCoroutine(Source.DeferredAction(lifeTime, Disintegrate));
        forward = transform.forward;
        MoveActive = true;
    }

    protected void Awake()
    {
        CliarEvents = false;      
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            Enemy enemy = other.GetComponent<Enemy>();
            enemy.SetDamage(Damage);
        }
        else if (other.tag == "Obstacle")
        {
            Disintegrate();
        }
    }

    protected override void move()
    {
        transform.position += forward * (Time.fixedDeltaTime * MoveSpeed);
    }

    public void Shoot()
    {
        gameObject.SetActive(true);
    }

    public void Shot(Vector3 position, Quaternion rotation)
    {
        transform.position = position;
        transform.rotation = rotation;

        Shoot();
    }

    public override void Disintegrate()
    {
        if (lifeCoroutine != null)
        {
            StopCoroutine(lifeCoroutine);
        }

        gameObject.SetActive(false);

        riseOnDisintegrate();
    }
}

