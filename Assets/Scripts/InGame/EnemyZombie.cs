﻿using UnityEngine;
using System.Collections;

public class EnemyZombie : Enemy
{
    protected override IEnumerator attack()
    {
        MoveActive = false;
        inAttack = true;

        while (Target && inAttack)
        {
            ThisAnimator.SetInteger("AttackType", Random.Range(0, 4));//единственное отличие от родителя

            ThisAnimator.SetBool("Attack", true);
            yield return new WaitForSeconds(HitDelay);
            ThisAnimator.SetBool("Attack", false);

            if (Target && (inAttack || GameSource.Instance.AnywayHit))
            {
                Target.SetDamage(Damage);
            }

            yield return new WaitForSeconds(ReloadTime - HitDelay);
        }
        inAttack = false;
        MoveActive = true;
    }
}
