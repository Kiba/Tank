﻿using UnityEngine;
using System.Collections;
using System;

public class GameConrtoller : MonoBehaviourSingleton<GameConrtoller>
{
    public float SpawnRadius = 100;

    public Player CurrentPlayer;

    protected override void Awake()
    {
        if (!CurrentPlayer) CurrentPlayer = FindObjectOfType<Player>();
        CurrentPlayer.OnDisintegrate += CurrentPlayer_OnDisintegrate;

        base.Awake();

        Spawn();
    }

    private void CurrentPlayer_OnDisintegrate(object sender, EventArgs e)
    {
        Time.timeScale = 0;
    }

    public int MaxEnemyCount = 10;
    private int enemyCount = 0;

    public void Spawn()
    {
        while (enemyCount < MaxEnemyCount)
        {
            enemyCount++;
            
            float angle = UnityEngine.Random.Range(0f, 360f);          
            float y = SpawnRadius * Mathf.Cos(angle);
            float z = SpawnRadius * Mathf.Sin(angle);
            Vector3 position = new Vector3(y, 0, z);

            Enemy enemy = Pull.Instance.GetRandomEnemy();
            enemy.transform.position = position;
            enemy.gameObject.SetActive(true);

            enemy.OnDisintegrate += Enemy_OnDisintegrate;
        }
    }

    private void Enemy_OnDisintegrate(object sender, EventArgs e)
    {
        enemyCount--;
        Spawn();
    }
}
