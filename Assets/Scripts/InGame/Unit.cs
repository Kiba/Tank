﻿using UnityEngine;

public abstract class Unit : MovementObject
{
    [Header("Броня - 0 отсутствие урона, 1 полный урон")]
    public float Armor = 0.5f;
    [Space(15)]

    public float MaxHitPoints = 10;

    public float HitPoints = 10;

    public virtual float SetDamage(float damage)
    {
        damage *= damage * Armor;
        HitPoints -= damage;
        if (HitPoints <= 0)
        {
            Disintegrate();
        }
        return damage;
    }    
}

