﻿
using System;
using System.Collections.Generic;

public class Pull : MonoBehaviourSingleton<Pull>
{
    private List<Enemy> enemys = new List<Enemy>();

    public void AddToPull(Enemy enemy)
    {
        enemy.gameObject.SetActive(false);

        enemys.Add(enemy);
    }

    public Enemy GetEnemy(string type)
    {
        Enemy toReturn = enemys.Find(item => item.EnemyType == type);
        if (!toReturn)
        {
            toReturn = GameSource.Instance.EnemyPrefabs.Find(item => item.EnemyType == type);
            if (toReturn)
            {
                toReturn = Instantiate(toReturn, transform) as Enemy;
            }
        }
        else
        {
            enemys.Remove(toReturn);
        }
        return toReturn;
    }

    public Enemy GetRandomEnemy()
    {
        if (enemys.Count == 0)
        {
            return Instantiate(GameSource.Instance.EnemyPrefabs.GetRandomItem(), transform) as Enemy;
        }

        Enemy toReturn = enemys.GetRandomItem();
        enemys.Remove(toReturn);
        return toReturn;
    }
}